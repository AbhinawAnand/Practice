package pageobjects;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;

import com.provar.core.testapi.annotations.*;

@SalesforcePage( title="UAT_ VF_ INSIDE_ VF"                                
               , summary=""
               , page="UAT_VF_INSIDE_VF"
               , namespacePrefix=""
               , object="Case"
               , connection="TestCycleAdmin"
     )             
public class UAT_VF_INSIDE_VF {

	WebDriver driver;
	public UAT_VF_INSIDE_VF(WebDriver driver){
	
	this.driver=driver;
	}

	@VisualforceBy(componentXPath = "apex:form//apex:inputField[@value = \"{!case.Currency__c}\"]")
	@SalesforceField(name = "Currency__c", object = "Case")
	public WebElement currency;
	@VisualforceBy(componentXPath = "apex:form//apex:inputField[@value = \"{!case.Type}\"]")
	@SalesforceField(name = "Type", object = "Case")
	public WebElement type;
	@BooleanType()
	@FindByLabel(label = "One")
	public WebElement one;
	@LinkType()
	@FindBy(linkText = "Google Search")
	public WebElement googleSearch;
	@VisualforceBy(componentXPath = "apex:include[@pagename='UAT_VF_Only']//apex:form//apex:inputField[@value = \"{!case.Currency__c}\"]")
	@SalesforceField(name = "Currency__c", object = "Case")
	public WebElement currency1;
	@LinkType()
	@FindBy(linkText = "31/08/2018")
	public WebElement testDate;
	@VisualforceBy(componentXPath = "apex:inputField[@value = \"{!case.TestMulti__c}\"]")
	@SalesforceField(name = "TestMulti__c", object = "Case")
	public WebElement testMulti;
	@VisualforceBy(componentXPath = "apex:inputField[@value = \"{!case.BusinessHoursId}\"]")
	@SalesforceField(name = "BusinessHoursId", object = "Case")
	public WebElement businessHours;
	@VisualforceBy(componentXPath = "apex:inputField[@value = \"{!case.AssetId}\"]")
	@SalesforceField(name = "AssetId", object = "Case")
	public WebElement asset;
	@VisualforceBy(componentXPath = "apex:inputField[@value = \"{!case.Reason}\"]")
	@SalesforceField(name = "Reason", object = "Case")
	public WebElement caseReason;
	@VisualforceBy(componentXPath = "apex:inputField[@value = \"{!case.IsEscalated}\"]")
	@SalesforceField(name = "IsEscalated", object = "Case")
	public WebElement escalated;
	@PageTable(row = Con.class)
	@VisualforceBy(componentXPath = "apex:pageblockTable[@id='pbd']")
	public List<Con> Con;
	@PageRow(byColumn = true)
	public static class Con {

		@TextType()
		@VisualforceBy(componentXPath = "apex:outputField[@value = \"{!a.lastname}\"]")
		public WebElement lastName;
		@BooleanType()
		@VisualforceBy(componentXPath = "apex:inputField[@value = \"{!a.donotcall}\"]")
		public WebElement checkbox;
		@ButtonType()
		@VisualforceBy(componentXPath = "apex:commandButton[@action='{!delcontact}']")
		public WebElement deleteButton;
	}
	@PageTable(row = Con1.class)
	@VisualforceBy(componentXPath = "apex:dataTable[@value = \"{!con}\"]")
	public List<Con1> Con1;
	@PageRow(byColumn = true)
	public static class Con1 {

		@LinkType()
		@VisualforceBy(componentXPath = "apex:column[2]/apex:outputField[@value = \"{!a.email}\"]/a")
		public WebElement email;
	}
	@PageTable(row = Con2.class)
	@VisualforceBy(componentXPath = "apex:pageBlock[not(@id)][5]/apex:pageBlockSection/apex:pageblockTable[@value = \"{!con}\"]")
	public List<Con2> Con2;
	@PageRow(byColumn = true)
	public static class Con2 {

		@PageTable(row = Con3.class)
		@VisualforceBy(componentXPath = "apex:column/apex:pageblockTable[@value = \"{!con}\"]")
		public List<Con3> Con3;
		@TextType()
		@FindByLabel(label = "Inner column 2")
		public WebElement innerColumn2;
	}
	@PageRow(byColumn = true)
	public static class Con3 {

		@LinkType()
		@VisualforceBy(componentXPath = "apex:outputField[@value = \"{!m.email}\"]/a")
		public WebElement innerColumn2;
	}
	
	public String returnValue(String xpath1, String action, String xpath2){
	
	String resultVaule = null;
	if(action.equalsIgnoreCase("Select"))
	{
	WebElement my_dropdown = driver.findElement(By.xpath(xpath1));
	Select my_Select = new Select(my_dropdown);
	my_Select.selectByIndex(0);
	resultVaule= driver.findElement(By.xpath(xpath2)).getText();
	}
	
	if(action.equalsIgnoreCase("DragandDrop"))
	{
	
		WebElement element_pos1 = driver.findElement(By.xpath(xpath1));
		WebElement element_pos2 = driver.findElement(By.xpath(xpath2));
		
		Actions builder = new Actions(driver);
		builder.dragAndDrop(element_pos1, element_pos2).perform();
		resultVaule = "Done";
	}
	
	
	if(action.equalsIgnoreCase("getValue"))
	{
		resultVaule = driver.findElement(By.xpath(xpath1)).getText();
		
	}
	
	return resultVaule;
	}
	
	
}
